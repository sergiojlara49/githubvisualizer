package com.examen.gitvisualizer.manager;

import android.content.Context;
import android.net.ConnectivityManager;

import java.io.IOException;

/**
 * Created by ryuzaki49 on 18/01/15.
 *
 */
public class ConnectionManager {

    public static boolean isConnected(Context context)  {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
}
