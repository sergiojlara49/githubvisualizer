package com.examen.gitvisualizer.manager;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ryuzaki49 on 18/01/15.
 *
 */
public class ImageManager {

    private String userName = "";
    private Bitmap bitmap;

    public ImageManager(String userName, Bitmap bitmap) {
        this.userName = userName;
        this.bitmap = bitmap;
    }

    public String saveImageInPath(){


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        String fileName = userName + ".jpg";
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "DCIM" + File.separator + fileName);

        FileOutputStream fo;
        try {
            f.createNewFile();
            fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
            return f.getAbsolutePath();
    }
}
