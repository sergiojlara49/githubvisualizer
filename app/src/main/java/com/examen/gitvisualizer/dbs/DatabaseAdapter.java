package com.examen.gitvisualizer.dbs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.examen.gitvisualizer.R;

/**
 * Created by ryuzaki49 on 18/01/15.
 *
 */
public class DatabaseAdapter {

    private static final String DATABASE_NAME = "examen.sqlite";
    private static final int DATABASE_SCHEME_VERSION = 1;

    public static final String TABLE_NAME = "GithubUser";
    public static final String CN_ID = "_id";
    public static final String CN_NAME = "Name";
    public static final String CN_LOCATION = "Location";
    public static final String CN_USERNAME = "Username";
    public static final String CN_RESOURCE = "Resource";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " (" + CN_ID + " integer primary key autoincrement," +
                                               CN_NAME + " text," + CN_USERNAME + " text not null," +
                                               CN_LOCATION + " text, " + CN_RESOURCE +" text);";

    public static final String[] ALL_COLUMNS = { CN_ID, CN_USERNAME, CN_NAME, CN_LOCATION, CN_RESOURCE};


    private  Context context;

    private DatabaseHelper myDBHelper;
    private SQLiteDatabase db;

    public DatabaseAdapter(Context context) {
        this.context = context;
        myDBHelper = new DatabaseHelper(context);
    }

    public DatabaseAdapter open() {
        db = myDBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        myDBHelper.close();
    }

    public long insertRow(String userName, String name, String location, String resourcePath ) {

        if(name.isEmpty() )
            name = context.getString(R.string.name_placeholder);
        if(location.isEmpty())
            location = context.getString(R.string.location_placeholder);
        ContentValues initialValues = new ContentValues();
        initialValues.put(CN_USERNAME, userName);
        initialValues.put(CN_NAME, name);
        initialValues.put(CN_LOCATION, location);
        initialValues.put(CN_RESOURCE, resourcePath);

        // Insert it into the database.
        return db.insert(TABLE_NAME, null, initialValues);
    }

    public boolean updateResourceRow(String userName, String resource) {
        String where = CN_USERNAME + "='" + userName + "'";
        ContentValues newValues = new ContentValues();
        newValues.put(CN_RESOURCE, resource);

        return db.update(TABLE_NAME, newValues, where, null) != 0;
    }

    public Cursor getRow(long rowId) {
        String where = CN_ID + "=" + rowId;
        Cursor c = 	db.query(true, TABLE_NAME, ALL_COLUMNS,
                where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public boolean userAlreadyInDatabase(String userName){

        String where = CN_USERNAME + "='" + userName + "'";
        Cursor c = db.query(false, TABLE_NAME, ALL_COLUMNS, where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }


        return c.getCount() > 0;
    }

    public Cursor getAllRows() {
        String where = null;
        Cursor c = 	db.query(true, TABLE_NAME, ALL_COLUMNS,
                where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_SCHEME_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DatabaseAdapter.CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }


}
