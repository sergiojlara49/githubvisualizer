package com.examen.gitvisualizer.server;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ryuzaki49 on 18/01/15.
 *
 */
public class LocationRequest extends AsyncTask<String, Void, Void> {

    private Listener listener;
    private double lat = 0, lng = 0, swLat = 0, swLng = 0, neLat = 0,  neLng = 0;

    public LocationRequest(Listener listener) {
        this.listener = listener;
    }

    JSONObject jsonObject;
    @Override
    protected Void doInBackground(String... params) {

        HttpClient httpClient = new DefaultHttpClient();
        String location = "";
       /* for( int i = 0; i < params.length; i++){
            location += params[i];
            if(i < params.length -1 )
                location += ",";
        }*/

        location = params[0].replaceAll("\\s","");

        Log.d("Util", "location: " + location + " params[0] " + params[0] );
        HttpGet httpGet = new HttpGet(Server.MAPS + location);
        HttpResponse response;
        try {
            response = httpClient.execute(httpGet);

            int responseCode = response.getStatusLine().getStatusCode();
            if(responseCode == 200){
                String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
                //Log.d("Util", " response " + responseString);
                jsonObject = new JSONObject(responseString);
                JSONArray jsonArray = jsonObject.getJSONArray("results");

                JSONObject locationJSON = jsonArray.getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                JSONObject southWestBounJSON = jsonArray.getJSONObject(0).getJSONObject("geometry").getJSONObject("bounds").getJSONObject("southwest");
                JSONObject northEastBoudJSON = jsonArray.getJSONObject(0).getJSONObject("geometry").getJSONObject("bounds").getJSONObject("northeast");

                lat = locationJSON.getDouble("lat");
                lng = locationJSON.getDouble("lng");
                swLat = southWestBounJSON.getDouble("lat");
                swLng = southWestBounJSON.getDouble("lng");
                neLat = northEastBoudJSON.getDouble("lat");
                neLng = northEastBoudJSON.getDouble("lng");

            }


        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(listener != null && lat != 0 && lng != 0){
            listener.didFetchCoordinates(lat, lng, swLat, swLng, neLat, neLng);
        }
    }

    public interface Listener{
        public void didFetchCoordinates(double latitude, double longitude, double swLatitude, double swLongitude, double neLatitude, double neLongitude);
    }
}
