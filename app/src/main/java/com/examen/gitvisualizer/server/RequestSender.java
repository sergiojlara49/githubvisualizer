package com.examen.gitvisualizer.server;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ryuzaki49 on 17/01/15.
 *
 */
public class RequestSender  extends AsyncTask<Void, Void, Void>{

    private String username;
    private String message = "";
    private JSONObject userJSON;
    private Listener listener;

    public RequestSender(String username, Listener listener) {
        this.username = username;
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Void... params) {

        DefaultHttpClient httpClient = new DefaultHttpClient();

        HttpGet httpGet = new HttpGet(Server.HOST + username);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);

            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if(responseCode == 200 ) {
                String responseString = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
                userJSON = new JSONObject(responseString);
            }
            else if(responseCode == 404)
                message = Server.NOT_FOUND;

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(listener != null) {
            if (message.equalsIgnoreCase(Server.NOT_FOUND))
                listener.userNotFound();
            else
                listener.didReceivedResponse(userJSON);
        }
    }


    public interface Listener{
        public void didReceivedResponse(JSONObject object);
        public void userNotFound();
    }
}
