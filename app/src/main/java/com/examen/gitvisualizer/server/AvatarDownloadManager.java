package com.examen.gitvisualizer.server;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.examen.gitvisualizer.manager.ImageManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ryuzaki49 on 18/01/15.
 *
 */
public class AvatarDownloadManager extends AsyncTask<String, Void, Void> {

    Listener listener;
    private String userName;
    Bitmap bitmap;
    public AvatarDownloadManager(String userName, Listener listener) {
        this.listener = listener;
        this.userName = userName;
    }

    @Override
    protected Void doInBackground(String... params) {

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(params[0]);
        HttpResponse response;
        InputStream inputStream = null;
        bitmap = null;
        try {
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                inputStream = entity.getContent();
                bitmap = BitmapFactory.decodeStream(inputStream);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

     finally {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        ImageManager imageManager = new ImageManager(userName, bitmap);
        String filePath = imageManager.saveImageInPath();
        if(listener != null)
            listener.didDownloadImage(filePath);
    }


    public interface Listener{
        public void didDownloadImage(String filePath);
    }
}
