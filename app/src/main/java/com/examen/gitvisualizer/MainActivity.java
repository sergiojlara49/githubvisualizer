package com.examen.gitvisualizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import com.examen.gitvisualizer.dbs.DatabaseAdapter;
import com.examen.gitvisualizer.manager.ConnectionManager;
import com.examen.gitvisualizer.server.AvatarDownloadManager;
import com.examen.gitvisualizer.server.RequestSender;

public class MainActivity extends ActionBarActivity {

    DatabaseAdapter myDb;
    private ListView githubUsersListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDb = new DatabaseAdapter(this);
        myDb.open();
        githubUsersListView = (ListView) findViewById(R.id.github_users_listview);

        Button addUserButton = (Button) findViewById(R.id.adduser_button);

       if(myDb.getAllRows().getCount() >0) {
           populateList();
           dissmissNoUsersScreen();
       }
        addUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConnectionManager.isConnected(getApplicationContext()))
                    createAlertDialog();
                else
                    Toast.makeText(getApplicationContext(), getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void dissmissNoUsersScreen() {
        findViewById(R.id.background_no_users).setVisibility(View.INVISIBLE);
        findViewById(R.id.textview_no_users).setVisibility(View.INVISIBLE);
    }


    private void createAlertDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_adduser, null);
        builder.setView(rootView)
                .setTitle(getString(R.string.add_user))
                .setPositiveButton(getString(R.string.accept), null)
                .setNegativeButton(getString(R.string.cancel), null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new CustomListener(alertDialog));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private EditText userNameEdit;

        public CustomListener(Dialog dialog) {
            this.dialog = dialog;
            userNameEdit = (EditText) dialog.findViewById(R.id.username_edit);
        }
        @Override
        public void onClick(View v) {
            // put your code here
            if(allFieldsHaveInput()) {
                searchUser();
            }
        }

        private void searchUser() {
            String userName = userNameEdit.getText().toString().trim();

            if (!myDb.userAlreadyInDatabase(userName)) {

                new RequestSender(userName, new RequestSender.Listener() {
                    @Override
                    public void didReceivedResponse(JSONObject object) {
                        try {
                            String userName = object.getString("login");
                            String name = object.getString("name");
                            String location = object.getString("location");
                            String avatarURL = object.getString("avatar_url");

                            addUser(name, userName, location, avatarURL);

                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void userNotFound() {
                        Toast.makeText(getApplicationContext(), getString(R.string.username_not_found), Toast.LENGTH_SHORT).show();
                    }
                }).execute();
            }
            else
                Toast.makeText(getApplicationContext(), getString(R.string.user_already_in_db), Toast.LENGTH_SHORT).show();
        }

        private boolean allFieldsHaveInput(){
            if(userNameEdit.getText().toString().trim().isEmpty()){
                Toast.makeText(getApplicationContext(), getString(R.string.username_not_entered), Toast.LENGTH_SHORT).show();
                return false;
            }
            else
                return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myDb.close();
    }

    private void addUser(String name, final String userName, String location, String avatarURL){
        myDb.insertRow(userName, name, location, "");

        if(!avatarURL.isEmpty()){
            new AvatarDownloadManager(userName, new AvatarDownloadManager.Listener() {
                @Override
                public void didDownloadImage(String filePath) {
                    myDb.updateResourceRow(userName, filePath);
                    populateList();
                    dissmissNoUsersScreen();

                }
            }).execute(avatarURL);
        }

        populateList();
        dissmissNoUsersScreen();
    }

    private void populateList(){

        Cursor cursor = myDb.getAllRows();
        String [] from = {DatabaseAdapter.CN_NAME, DatabaseAdapter.CN_USERNAME, DatabaseAdapter.CN_LOCATION, DatabaseAdapter.CN_RESOURCE};
        int [] to = {R.id.name_textview, R.id.username_textview, R.id.location_textview, R.id.avatar_view};
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, from, to);
        githubUsersListView.setAdapter(adapter);

        githubUsersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = myDb.getRow(position +1 );
                String childUserName = c.getString(1);
                String childName = c.getString(2);
                String childLocation = c.getString(3);
                if(!childLocation.equalsIgnoreCase("Desconocida")){
                    Intent intent = new Intent(MainActivity.this, MapActivity.class);
                    intent.putExtra("location", childLocation);
                    startActivity(intent);
                }
                else
                Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.user_no_map)+ childLocation, Toast.LENGTH_SHORT).show();

            }
        });

    }
}
