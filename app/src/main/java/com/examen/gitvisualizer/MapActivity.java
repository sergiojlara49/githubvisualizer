package com.examen.gitvisualizer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.examen.gitvisualizer.server.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by ryuzaki49 on 18/01/15.
 *
 */
public class MapActivity extends Activity {

    MapView mapView;
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mapView = (MapView) findViewById(R.id.google_map);
        mapView.onCreate(savedInstanceState);

        MapsInitializer.initialize(this);
        googleMap = mapView.getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        Intent intent = getIntent();
        String location = intent.getStringExtra("location");

        String [] params = location.split(",");

        Toast.makeText(getApplicationContext(), params[0], Toast.LENGTH_SHORT).show();
        new LocationRequest(new LocationRequest.Listener() {
            @Override
            public void didFetchCoordinates(double latitude, double longitude, double swLatitude,
                                            double swLongitude, double neLatitude, double neLongitude) {
                LatLng studentPosition= new LatLng(latitude, longitude);
                Marker studentMarker = googleMap.addMarker(new MarkerOptions()
                        .position(studentPosition)
                        .draggable(false));

                LatLng southWest = new LatLng(swLatitude, swLongitude);
                LatLng northEast = new LatLng(neLatitude, neLongitude);
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder().include(southWest).include(northEast);
                boundsBuilder.include(studentPosition);

                final LatLngBounds bounds = boundsBuilder.build();
                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
                    }
                });
            }
        }).execute(location);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
